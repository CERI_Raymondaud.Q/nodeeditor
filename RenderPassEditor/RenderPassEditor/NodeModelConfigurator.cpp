# include "NodeModelConfigurator.h"

// FILE SCOPED VARIABLES 
static int in = 1 ; 				// Nb input showed in configurator by default
static int out = 1 ; 				// Nb output ...
static char nodeName [20] = "" ; 	// Node Name
static bool invalid = false ;		// True if user tries to create an invalid node
static bool removeParams = true ;	// Default Remove params content in configurator after creating a node

// 10 I/O identified by 10 char
static char inputs [10] [10] {"", "", "", "", "", "", "", "", "", ""} ;
static char outputs [10] [10] {"", "", "", "", "", "", "", "", "", ""} ;
static int nodeId = 1 ;


// / ! \ --- English comments version into header 

// Fenetre d'interface ImGui de création de noeud
void NodeCreator (NodeModelsHandler & nodes) { 

	ImGui::SetWindowSize (ImVec2 (576, 680) , 100.f) ;
	ImGui::Begin (" Node Configurator ", NULL, ImGuiWindowFlags_AlwaysAutoResize) ;

	if (ImGui::Button ("Create Node") ) 
		create_node (nodes) ;

	if (invalid) 
		invalid_node () ;

	ImGui::SameLine () ;

	// POP a node from nodes model handler ( NOT PROPER , DO NOT DELETE LINKS )
	if (ImGui::Button ("Delete Last Node") )
		delete_last_node (nodes) ;

	ImGui::SameLine () ;

	// BROKEN ! ...
	if (ImGui::Button ("Delete Seleted Nodes") ) {

		for ( ed::NodeId id : nodes.selectedNodes ) {
			std::cout << " NodeModelConfig.cpp : Deleted Node ID " <<  std::to_string (reinterpret_cast<uintptr_t> (id.AsPointer () ) ).c_str () << std::endl;
	    	nodes.delete_node (id) ;
		}
	}

	ImGui::Checkbox ("Remove params after creation ?", &removeParams) ;

	 // I/O number Sliders
	ImGui::SliderInt ("Nb Input", &in, 1, 10) ;
	ImGui::SliderInt ("Nb Output", &out, 1, 10) ;

	// Node name 
	ImGui::InputTextWithHint ("Name", "RenderPass Name", nodeName, IM_ARRAYSIZE (nodeName) ) ;

	display_parameters () ;

	ImGui::End () ;

}

// / ! \ --- English comments version into header 

// Affiche les champs de configuration des params
// En fonction des valeurs des sliders qui définissent le nombre d'I/O
void display_parameters () {

	int max = std::max (in, out) ;
	int tmpIn, tmpOut ;
	tmpIn = tmpOut = 0 ;
	std::string inId = "" ;
	std::string outId = "" ;
	ImGui::Columns (2, nullptr, false) ;
	for (int i = 0 ; i < max ; i ++) { 				// For the max number between nbIn && nbOut

		if (tmpIn < in) {							// Creating textfield if input required 									
			inId = "In " + std::to_string ((tmpIn ++) + 1) ;
			ImGui::Text ("%s", inId.c_str () ) ;
			ImGui::SameLine () ;
			inId = "##" + inId ;
			ImGui::InputTextWithHint (inId.c_str () , "Input Name", inputs [tmpIn-1] , IM_ARRAYSIZE (inputs [tmpIn - 1] ) ) ;
		}

		ImGui::NextColumn () ; 							// Go to Right Column  (The second) 

		if  (tmpOut < out)  {							// Creating textfield if output required
			outId = "Out " + std::to_string ((tmpOut++)  + 1) ;
			ImGui::Text ("%s", outId.c_str () ) ;
			ImGui::SameLine () ;
			outId = "##" + outId ;
			ImGui::InputTextWithHint (outId.c_str () , "Output Name", outputs [tmpOut - 1] , IM_ARRAYSIZE (outputs [tmpOut - 1] ) ) ;
		}

		ImGui::NextColumn () ;							// Go to Left Column  (The first)
	}
}

// / ! \ --- English comments version into header 

// Affiche un popup d'avertissement 
void invalid_node () {
	ImGui::OpenPopup ("Invalid Node") ;
	if  (ImGui::BeginPopupModal ("Invalid Node", &invalid) ) 
		ImGui::Text (" A node takes at least : \n   -A Name\n   -1 Input\n   -1 Output") ;
	if  (ImGui::Button ("Close") ) {
		invalid = false ;
		ImGui::CloseCurrentPopup () ;
	}
	ImGui::EndPopup () ;
}


// / ! \ --- English comments version into header 

// Copie les informations et créer
// le noeud dans le NodeModelsHandler
void create_node (NodeModelsHandler & nodes) {
	std::vector< std::string > vec_inputs, vec_outputs ;
	int nbIn, nbOut ;
	nbIn = nbOut = 0 ;

	for  (char * i : inputs) 						// Crossing input array ...
		if  (strcmp (i, "")  != 0 && nbIn < in)		// If param not empty and not overselected
		{
			vec_inputs.push_back (i) ;
			nbIn ++ ;
		}

	for  (char * o : outputs) 						// Crossing output array ...
		if (strcmp (o, "")  != 0 && nbOut < out) 	// If param not empty and not overselected
		{
			vec_outputs.push_back (o) ;
			nbOut++ ;
		}

	if ( (nbIn != 0) && (nbOut != 0) && (strcmp (nodeName, "") != 0) ) // Node creation if valid
	{
		invalid = false ;
		std::cout << " NodeModelConfig.cpp : Created Node Id : " << nodeId << std::endl;
		NodeModel newNode (ed::NodeId (nodeId ++) , nodeName, vec_inputs, vec_outputs, nbIn, nbOut) ;
		nodes.add (newNode) ;

		if (removeParams) // Params removal
		{
			strncpy (nodeName, "", 1) ;
			for (int i = 0 ; i < 10 ; i ++) 
			{
				strncpy (inputs [i] , "", 1) ;
				strncpy (outputs [i] , "", 1) ;
			}
		}

	}
	else // Node invalidation 
		invalid = true ;
}

void delete_last_node (NodeModelsHandler & nodes) { nodes.pop () ; }

void delete_node (NodeModelsHandler & nodes, ed::NodeId id) { nodes.delete_node (id) ; }